package com.example.goger.myapplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.util.Pair;
import android.util.Size;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.places.Places;

import android.support.v4.app.FragmentActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;


public class MainActivity extends FragmentActivity implements OnConnectionFailedListener {

    private final static String TAG = "SimpleCamera";
    private TextureView mTextureView = null;
    private GoogleApiClient mGoogleApiClient;
    private static double thetaV, thetaH, angleX, angleY, angleZ;

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    private static final String[] CONTACTS_PERMS = {
            Manifest.permission.READ_CONTACTS
    };
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int CAMERA_REQUEST = INITIAL_REQUEST + 1;
    private static final int CONTACTS_REQUEST = INITIAL_REQUEST + 2;
    private static final int LOCATION_REQUEST = INITIAL_REQUEST + 3;
    private static float[] mGravity = null;
    private static float[] mGeomagnetic = null;

    private static double latitude = 0;
    private static double longitude = 0;
    JSONObject nearby_places = null;
    ArrayList<Pair<Integer, String>> names = null;

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public static double distanceTo(double lat, double lng){

        int EARTH_RADIUS_KM = 6371;
        double lat1Rad = Math.toRadians(latitude);
        double lat2Rad = Math.toRadians(lat);
        double deltaLonRad = Math.toRadians(lng - longitude);

        return 1000*Math.acos(
                Math.sin(lat1Rad) * Math.sin(lat2Rad) +
                        Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.cos(deltaLonRad)
        ) * EARTH_RADIUS_KM;
    }

    public void updatePlaces() throws JSONException {
        if (nearby_places != null) {
            JSONArray places = nearby_places.getJSONArray("results");

            ListView lvMain = (ListView) findViewById(R.id.listView);
            if (lvMain != null) {

                names = new ArrayList<>();
                for (int i = 0; i < places.length(); i++) {
                    JSONObject c = places.getJSONObject(i);
                    String name = c.getString("name");
                    double place_latitude = c.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                    double place_longitude = c.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                    // Log.i(TAG, "Google said name: " + name);
                    // Log.i(TAG, "Google said lat: " + place_latitude);
                    // Log.i(TAG, "Google said lng: " + place_longitude);

                    double angle_with_place = Math.acos((place_longitude - longitude) /
                            Math.sqrt((place_longitude - longitude) * (place_longitude - longitude)
                                    + (place_latitude - latitude) * (place_latitude - latitude)));
                    if (place_latitude - latitude > 0)
                        angle_with_place *= -1;

                    double from = angleX - thetaH / 2;
                    double to = angleX + thetaH / 2;

                    if (from > to)
                        to += Math.PI * 2;
                    Log.i(TAG, "" + angleX + " " + angleY + " " + angleZ);
                    if ((angle_with_place < to && angle_with_place > from) ||
                            (angle_with_place + Math.PI * 2 < to && angle_with_place + Math.PI * 2 > from) ||
                            (angle_with_place - Math.PI * 2 < to && angle_with_place - Math.PI * 2 > from)) {
                        names.add(new Pair<>((int) distanceTo(place_latitude, place_longitude), name));
                    }
                }

                Collections.sort(names, new Comparator<Pair<Integer, String>>() {
                    @Override
                    public int compare(Pair<Integer, String> o1, Pair<Integer, String> o2) {
                        if (o1.first < o2.first)
                            return -1;
                        if (o1.first > o2.first)
                            return 1;
                        return 0;
                    }
                });
                ArrayList<String> names_ordered = new ArrayList<>();
                for (Pair<Integer, String> name : names) {
                    names_ordered.add(name.second + " (" + name.first + " м)");
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
                        R.layout.listview, R.id.textView, names_ordered.toArray(new String[names_ordered.size()]));

                lvMain.setAdapter(adapter);
            }
        }
    }

    private class GetUrlContentTask extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... params) {
            URL url = null;
            try {
                url = new URL("https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyBbCilh5Ox6Y4CI3oXMKQDOoQtEEKm24HQ&location=" + latitude + "," + longitude + "&radius=1000&type=point_of_interest");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            // Log.i(TAG, "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyBbCilh5Ox6Y4CI3oXMKQDOoQtEEKm24HQ&location=" + latitude + "," + longitude + "&radius=1000&type=point_of_interest");
            HttpURLConnection connection = null;

            boolean connected = false;
            while (!connected)
                try {
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.setDoOutput(true);
                    connection.setConnectTimeout(5000);
                    connection.setReadTimeout(5000);
                    connection.connect();
                    connected = true;
                } catch (IOException e) {
                    e.printStackTrace();
//                    SystemClock.sleep(2000);
                }
            BufferedReader rd = null;
            try {
                rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
//                SystemClock.sleep(2000);
            }
            StringBuilder content = new StringBuilder();
            String line;
            try {
                while ((line = rd.readLine()) != null) {
                    content.append(line).append("\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return content.toString();
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        protected void onPostExecute(String result) {
            // this is executed on the main thread after the process is over
            // update your UI here
            try {
                setContentView(R.layout.activity_main);

                mTextureView = (TextureView) findViewById(R.id.textureView1);
                mTextureView.setSurfaceTextureListener(mSurfaceTextureListner);
                nearby_places = new JSONObject(result);
                updatePlaces();
            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());
            }
        }
    }

    private class MyLocationListener implements LocationListener {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onLocationChanged(Location loc) {
            //pb.setVisibility(View.INVISIBLE);
//            setContentView(R.layout.activity_loading);
            try {
                TextView textView = findViewById(R.id.loadingText);
                textView.setText("Завантаження даних про навколишні місця...");
            } catch (Exception e) {

            }

            new GetUrlContentTask().execute();
            latitude = loc.getLatitude();
            longitude = loc.getLongitude();

            String longitudes = "Longitude: " + loc.getLongitude();
            Log.v(TAG, longitudes);
            String latitudes = "Latitude: " + loc.getLatitude();
            Log.v(TAG, latitudes);

//            setContentView(R.layout.activity_main);
//
//            mTextureView = (TextureView) findViewById(R.id.textureView1);
//            mTextureView.setSurfaceTextureListener(mSurfaceTextureListner);
//
//            String s = longitudes + "\n" + latitudes;
//            try {
//                updatePlaces();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    private TextureView.SurfaceTextureListener mSurfaceTextureListner = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            // TODO Auto-generated method stub
            //// Log.i(TAG, "onSurfaceTextureUpdated()");

        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width,
                                                int height) {
            // TODO Auto-generated method stub
            // Log.i(TAG, "onSurfaceTextureSizeChanged()");

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            // TODO Auto-generated method stub
            // Log.i(TAG, "onSurfaceTextureDestroyed()");
            return false;
        }

        @SuppressLint("MissingPermission")
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width,
                                              int height) {
            // TODO Auto-generated method stub
            // Log.i(TAG, "onSurfaceTextureAvailable()");

            CameraManager manager = (CameraManager) getSystemService(CAMERA_SERVICE);
            try {
                assert manager != null;
                String cameraId = manager.getCameraIdList()[0];
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                assert map != null;
                mPreviewSize = map.getOutputSizes(SurfaceTexture.class)[0];

                manager.openCamera(cameraId, mStateCallback, null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }

        }
    };

    private Size mPreviewSize = null;
    private CameraDevice mCameraDevice = null;
    private CaptureRequest.Builder mPreviewBuilder = null;
    private CameraCaptureSession mPreviewSession = null;
    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            // TODO Auto-generated method stub
            // Log.i(TAG, "onOpened");
            mCameraDevice = camera;

            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            if (texture == null) {
                Log.e(TAG, "texture is null");
                return;
            }

            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            Surface surface = new Surface(texture);

            try {
                mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }

            mPreviewBuilder.addTarget(surface);

            try {
                mCameraDevice.createCaptureSession(Collections.singletonList(surface), mPreviewStateCallback, null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            // TODO Auto-generated method stub
            Log.e(TAG, "onError");

        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            // TODO Auto-generated method stub
            Log.e(TAG, "onDisconnected");

        }
    };
    private CameraCaptureSession.StateCallback mPreviewStateCallback = new CameraCaptureSession.StateCallback() {

        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            // TODO Auto-generated method stub
            // Log.i(TAG, "onConfigured");
            mPreviewSession = session;

            mPreviewBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

            HandlerThread backgroundThread = new HandlerThread("CameraPreview");
            backgroundThread.start();
            Handler backgroundHandler = new Handler(backgroundThread.getLooper());

            try {
                mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), null, backgroundHandler);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
            // TODO Auto-generated method stub
            Log.e(TAG, "CameraCaptureSession Configure failed");
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
        }

        Camera.Parameters p = Camera.open().getParameters();
        thetaV = Math.toRadians(p.getVerticalViewAngle());
//        thetaH = Math.toRadians(p.getHorizontalViewAngle());
        thetaH = 0.25;
        // Log.i(TAG, "V: " + thetaV);
        // Log.i(TAG, "H: " + thetaH);

//		new GetUrlContentTask().execute();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_loading);
        TextView textView = findViewById(R.id.loadingText);
        textView.setText("Завантаження місця розташування...");

//        mTextureView.setSurfaceTextureListener(mSurfaceTextureListner);
//		ArrayList<String> names = new ArrayList<String>();
//		names.add("123");
//		names.add("456");
//		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
//				R.layout.listview, R.id.textView, names.toArray(new String[names.size()]));
//
//
//		lvMain.setAdapter(adapter);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new MyLocationListener();
        assert locationManager != null;
        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 15000, 10f, locationListener);

        SensorManager sensorManager =
                (SensorManager) getSystemService(SENSOR_SERVICE);

        assert sensorManager != null;
        Sensor accelerometerSensor =
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor magneticSensor =
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);


        SensorEventListener gyroscopeSensorListener = new SensorEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
                    mGravity = sensorEvent.values.clone();

                if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                    mGeomagnetic = sensorEvent.values.clone();

                if (mGravity != null && mGeomagnetic != null) {
                    float R[] = new float[9];
                    float I[] = new float[9];

                    boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
                    if (success) {
                        float[] outR = new float[9];
                        float orientation[] = new float[3];
                        SensorManager.remapCoordinateSystem(R, SensorManager.AXIS_X,SensorManager.AXIS_Z, outR);
                        SensorManager.getOrientation(outR, orientation);

                        angleX = -47.0416107178 / 180 * Math.PI + orientation[0]; //looks like we don't need this one
                        if (angleX < -Math.PI)
                            angleX += 2 * Math.PI;
                        angleY =orientation[1];
                        angleZ = orientation[2];
                        mGeomagnetic = null; //retrigger the loop when things are repopulated
                        mGravity = null; ////retrigger the loop when things are repopulated

//                        SensorManager.getOrientation(R, orientation);
//                        angleX = orientation[0]; // orientation contains: azimut, pitch and roll
//                        angleY = orientation[1];
//                        angleZ = orientation[2];
                        // Log.i(TAG, "x-angle: " + angleX);
                        // Log.i(TAG, "y-angle: " + angleY);
                        // Log.i(TAG, "z-angle: " + angleZ);
                        try {
                            updatePlaces();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                // More code goes here
//				Toast.makeText(
//						getBaseContext(),
//						"Angle speed changed: x: " + sensorEvent.values[0] + " y: "
//								+ sensorEvent.values[1] + " z: " + sensorEvent.values[2], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }
        };

        // Register the listener
        sensorManager.registerListener(gyroscopeSensorListener,
                accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(gyroscopeSensorListener,
                magneticSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        if (mCameraDevice != null) {
            mCameraDevice.close();
            mCameraDevice = null;
        }
    }

}
